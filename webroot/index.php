<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Casablanca</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Casablanca is one page website build from PSD file, it is made purely for educational purpose.">
    <meta name="keywords" content="#">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/styles.css">
</head>
<body>
<header>
    <nav class="navigation">
        <a href="#" class="menu-icon"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="nav flex-column main-navigation">
            <li class="nav-item"><a class="nav-link" href="#hero">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="#introduction">Introduction</a></li>
            <li class="nav-item"><a class="nav-link" href="#creativity">Creativity</a></li>
            <li class="nav-item"><a class="nav-link" href="#work">Work</a></li>
            <li class="nav-item"><a class="nav-link" href="#help">Help</a></li>
            <li class="nav-item"><a class="nav-link" href="#clients">Clients</a></li>
            <li class="nav-item"><a class="nav-link" href="#about">About</a></li>
            <li class="nav-item"><a class="nav-link" href="#choice">Video</a></li>
            <li class="nav-item"><a class="nav-link" href="#chose-us">Why us</a></li>
            <li class="nav-item"><a class="nav-link" href="#quote">Quote</a></li>
            <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>
        </ul>
        <div>
            <a href="#">Close</a>
        </div>
    </nav>
</header>
<!-- end navigation -->

<!-- section hero -->
<section id="hero" class="hero">
    <div class="hero-overlay"></div>
    <div class="container">
        <div class="hero-text-position">
            <h1 class="text-uppercase">We've got the talent</h1>
            <hr>
            <h2>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</h2>
        </div>
    </div>
</section>
<!-- end of section -->

<!-- section introduction -->
<section id="introduction" class="introduction">
    <div class="container">
        <h3 class="display-5 text-uppercase">Introduction</h3>
        <hr class="hr2">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
    </div>
</section>
<!-- end section introduction-->

<!-- section creativity -->
<section id="creativity" class="creativity">
    <div class="container">
        <a href="#" class="btn btn-secondary btn-lg">Pure Creativity.</a> <!-- block pure creativity -->
    </div>
</section>
<!-- end  section-->

<!-- section some works -->
<section id="work" class="work">
    <div class="container">
        <div>
            <h3 class="display-5 text-uppercase">Some of our work</h3>
            <hr class="hr2"> <!-- black line separator -->
        </div>
        <nav class="text-uppercase">
            <ul>
                <li><a href="#work">All</a></li>
                <li><a href="#work">Branding</a></li>
                <li><a href="#work">Web design</a></li>
                <li><a href="#work">Mobile ui</a></li>
                <li><a href="#work">Illustrations</a></li>
            </ul>
        </nav>
        <nav>
            <ul class="row work-items">
                <li class="col-md">
                    <img class="img-fluid" src="images/design/art_1.jpg" alt=""/>
                    <div class="img-work">
                        <div class="work-img-centered ">
                            <h3>Branding</h3>
                            <p>MTV AMA logo</p>
                            <hr class="hr-orange">
                        </div>
                    </div>
                </li>
                <li class="col-md">
                    <img class="img-fluid" src="images/design/art_2.jpg" alt=""/>
                    <div class="img-work">
                        <div class="work-img-centered">
                            <h3>Illustrations</h3>
                            <p>Redesign</p>
                            <hr class="hr-orange">
                        </div>
                    </div>
                </li>
                <li class="col-md">
                    <img class="img-fluid" src="images/design/art_3.jpg" alt=""/>
                    <div class="img-work">
                        <div class="work-img-centered">
                            <h3>Mobile UI</h3>
                            <p>Google Mail</p>
                            <hr class="hr-orange">
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="row work-items">
                <li class="col-md">
                    <img class="img-fluid" src="images/design/art_4.jpg" alt=""/>
                    <div class="img-work">
                        <div class="work-img-centered">
                            <h3>Illustrations</h3>
                            <p>Portico redesign</p>
                            <hr class="hr-orange">
                        </div>
                    </div>
                </li>
                <li class="col-md">
                    <img class="img-fluid" src="images/design/art_5.jpg" alt=""/>
                    <div class="img-work">
                        <div class="work-img-centered">
                            <h3>Branding</h3>
                            <p>Smash logo</p>
                            <hr class="hr-orange">
                        </div>
                    </div>
                </li>
                <li class="col-md">
                    <img class="img-fluid" src="images/design/art_6.jpg" alt=""/>
                    <div class="img-work">
                        <div class="work-img-centered">
                            <h3>Web Design</h3>
                            <p>Salt site</p>
                            <hr class="hr-orange">
                        </div>
                    </div>
                </li>
            </ul>
            <a href="#" class="btn btn-secondary btn-lg">Show me more</a> <!-- block button show me more -->
        </nav>
    </div>
</section>
<!-- end section -->

<!-- section help -->
<section id="help" class="help" >
    <div class="container">
        <a href="#" class="btn btn-secondary btn-lg">We’re here to help.</a>
    </div>
</section>
<!-- end section -->

<!-- section clients -->
<section id="clients" class="clients">
    <div class="container">
        <h3 class="display-5 text-center">Our clients</h3>
        <hr class="hr2"> <!-- our client separator -->
        <ul class="row">
            <li class="col-sm"><a href=""><img src="images/logos/google.jpg" alt=""/></a></li>
            <li class="col-sm"><a href=""><img src="images/logos/samsung.jpg" alt=""/></a></li>
            <li class="col-sm"><a href=""><img src="images/logos/flickr.jpg" alt=""/></a></li>
            <li class="col-sm"><a href=""><img src="images/logos/fsq.jpg" alt=""/></a></li>
            <li class="col-sm"><a href=""><img src="images/logos/p.jpg" alt=""/></a></li>
        </ul>
    </div>
</section>
<!-- end section -->

<div class="clearfix"></div>

<!-- section about -->
<section id="about" class="about">
    <div class="container">
        <h3 class="display-5">About Us</h3>
        <hr class="hr2"> <!-- you know this is separator -->
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt utlabore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris. </p>
        <ul class="row">
            <li class="img-about">
               <div class="col">
                   <img src="images/employees/pep1.jpg" />
                   <div class="overlay-about">
                       <a href="#"><i class="fa fa-external-link"></i></a>
                       <div class="img-text-about">
                           <h3>JOHN</h3>
                           <p>CEO/Founder</p>
                       </div>
                   </div>
               </div>
            </li>
            <li class="img-about">
                <div class="col">
                    <img src="images/employees/pep2.jpg" />
                    <div class="overlay-about">
                        <a href="#"><i class="fa fa-external-link"></i></a>
                        <div class="img-text-about">
                            <h3>WILIAN</h3>
                            <p>CTO/Founder</p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="img-about">
                <div class="col">
                    <img src="images/employees/pep3.jpg" />
                    <div class="overlay-about">
                        <a href="#"><i class="fa fa-external-link"></i></a>
                        <div class="img-text-about">
                            <h3>CLARE</h3>
                            <p>Designer</p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="hiring">
                <div class="hiring-text">
                    <h5 class="text-uppercase">We are hiring</h5>
                </div>
                <div class="hiring-btn">
                    <a href="#">Apply</a>
                </div>
            </li>
        </ul>
    </div>
</section>
<!-- end about -->

<!-- section choice -->
<section id="choice" class="choice">
    <div >
        <a href="https://www.youtube.com/watch?v=N8GksI_-iIM"><i class="fa fa-play-circle"></i></a> <!-- video link with some overlay effect -->
    </div>
</section>
<!-- end choice -->

<!-- chose us section-->
<section id="chose-us" class="chose-us">
      <div class="container">
          <h3 class="display-5">Why choose us</h3>
          <hr class="hr2"> <!-- once again separator -->
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt utlabore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
          <ul class="row">
              <li class="col">
                  <img class="img-fluid" src="images/services/iphone.jpg" alt=""/>
                  <h5>App Design</h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporilabore et dolore.</p>
              </li>
              <li class="col">
                  <img class="img-fluid" src="images/services/ipad.jpg" alt=""/>
                  <h5>App Design</h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporilabore et dolore.</p>
              </li>
              <li class="col">
                  <img class="img-fluid" src="images/services/mac.jpg" alt=""/>
                  <h5>App Design</h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporilabore et dolore.</p>
              </li>
          </ul>
      </div>
</section>
<!-- end section -->

<!-- section quote -->
<section id="quote" class="quote">
    <div class="container">
        <a href="#" class="btn btn-secondary btn-lg">Get a quote now.</a>
    </div>
</section>
<!-- end quote -->

<!-- section get in touch -->
<section id="touch" class="touch">
    <div class="container">
        <h3 class="display-5">Get in touch</h3>
        <hr class="hr2"> <!-- once again separator -->
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt utlabore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
    </div>
</section>
<!-- end -->

<!-- location -->
<section class="contact" id="contact">
       <address class="contact-location">
           <div class="location text-left">
               <h5 class="display-5 text-uppercase">Our location</h5>
               <div itemscope itemtype="http://schema.org/ContactPoint">
                   <div itemscope itemtype="schema.org/PostalAddress">
                       <span itemprop="streetAddress">74 Mimosa ST. NW U</span>
                       <span itemprop="postalCode">Casablanca, MA 20370</span>
                   </div>
                   <span itemprop="telephone">(212) 123 456 7</span>
               </div>
               <p>Samir Timezguida</p>
               <ul class="text-left list-unstyled">
                   <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                   <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                   <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                   <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                   <li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
               </ul>
           </div>
       </address>
    <div class="form">
        <div class="form-group">
            <form class="form-inline form-check-inline">
                <input type="name" class="form-control"  placeholder="Name">
                <p style="padding-right:30px;"></p>
                <input type="email" class="form-control"  placeholder="Email">
                <div style="padding: 30px"></div>
            </form>
            <form class="bottom">
                <input type="email" class="form-control"  placeholder="Message">
            </form>
            <a href="#" class="btn btn-secondary btn-lg text-center">Sign in</a>
        </div>
    </div>
    </div>
</section>
<!-- end section -->

<div class="clearfix"></div>
<!-- footer -->
<footer id="footer">
    <div class="container">
        <a href="#"><img src="images/logo.png" alt=""/></a>
        <p>2014 &copy;<a href="#">Samir Timezguida</a> All Rights Reserved</p>
    </div>
</footer>
<!-- end of footer -->

<script src="/js/jquery.2.1.3.min.js"></script>
<script src="/js/jquery.min.js"></script>
<script src="/js/singlenav.js"></script>
<script src="/js/scripts.js"></script>
<script src="/js/tether.min.js"></script>
<script src="/js/bootstrap.min.js"></script>

</body>
</html>
